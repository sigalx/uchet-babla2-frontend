<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class ImageSelectAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/image-select';

    public $css = [
        'ImageSelect.css',
    ];

    public $js = [
        'ImageSelect.jquery.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

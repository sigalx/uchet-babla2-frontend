<?php

namespace frontend\controllers;

use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\authclient\ClientInterface;
use yii\base\UserException;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\widgets\ActiveForm;

use frontend\components\ApiCallException;

use frontend\models\User;
use frontend\models\LoginForm;
use frontend\models\EmailForm;
use frontend\models\CreateTransferForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    protected $_authNeededActions = array(
        'summary-arrears',
        'transfer-log',
        'create-transfer',
        'logout'
    );

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [[
                    'actions' => $this->_authNeededActions,
                    'allow' => false,
                    'roles' => ['?'],
                ], [
                    'actions' => $this->_authNeededActions,
                    'allow' => true,
                    'roles' => ['@'],
                ], [
                    'allow' => true,
                ],],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'validate-number' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthServiceSuccess'],
            ],
        ];
    }

    public function onAuthServiceSuccess(ClientInterface $client)
    {
        $attributes = $client->getUserAttributes();
        $authService = null;

        try {
            $authService = Yii::$app->apiClient->apiCall('findAuthServiceLink', [
                'source' => $client->getId(),
                'source_id' => $attributes['id'],
            ]);
        } catch (ApiCallException $exception) {
        }

        if (Yii::$app->user->isGuest) {
            $user = new User();
            if ($authService) { // login
                $user->setUserInfo($authService->user_info);
                Yii::$app->user->login($user);
                return true;
            }
            // signup
//            if (isset($attributes['email']) && User::find()->where(['email' => $attributes['email']])->exists()) {
//                Yii::$app->getSession()->setFlash('error', [
//                    Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", [
//                        'client' => $client->getTitle(),
//                    ]),
//                ]);
//                return false;
//            }
            $newUser = null;
            $login = $client->getId() . '-' . (string)$attributes['id'];
            $username = null;
//            if (isset($attributes['email'])) {
//                $user->email = $attributes['email'];
//            }
            if (isset($attributes['displayName'])) {
                $username = $attributes['displayName'];
            }
            if (isset($attributes['first_name']) && isset($attributes['last_name'])) {
                $username = $attributes['first_name'] . ' ' . $attributes['last_name'];
            }
            try {
                $newUser = Yii::$app->apiClient->apiCall('createUser', [
                    'login' => $login,
                    'username' => $username,
                ]);
            } catch (ApiCallException $apiCallException) {
                $newUser = Yii::$app->apiClient->apiCall('createUser', [
                    'login' => $login . '-' . substr(md5($login), 0, 6),
                    'username' => $username,
                ]);
            }
            Yii::$app->apiClient->apiCall('createAuthServiceLink', [
                'user_id' => $newUser->user_info->id,
                'source' => $client->getId(),
                'source_id' => (string)$attributes['id'],
            ]);
            $user->setUserInfo($newUser->user_info);
            Yii::$app->user->login($user);
            return true;
        } else { // user already logged in
            if (!$authService) { // add auth provider
                Yii::$app->apiClient->apiCall('createAuthServiceLink', [
                    'user_id' => Yii::$app->user->id,
                    'source' => $client->getId(),
                    'source_id' => (string)$attributes['id'],
                ]);
            }
        }
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            $this->layout = 'mainNoTitle';
            return $this->render('indexGuest');
        }
        if (empty(\Yii::$app->user->identity->userInfo->email)) {
            $this->layout = 'mainNoTitle';
            return $this->actionSetEmail();
        }
        return $this->actionSummaryArrears();
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'mainNoTitle';
            return $this->render('indexGuest', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSetEmail()
    {
        $model = new EmailForm();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax && Yii::$app->request->post('ajax') == 'email-form') {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->validate()) {
                Yii::$app->apiClient->apiCall('updateUserInfo', [
                    'user_id' => Yii::$app->user->id,
                    'user_info' => [
                        'email' => $model->email,
                    ],
                ]);
                return $this->redirect(['/site/create-transfer']);
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionUpdateTargetAmount()
    {
        /** @var CreateTransferForm $model */
        $model = null;
        if (isset(Yii::$app->session['currentTransfer'])) {
            $model = unserialize(Yii::$app->session['currentTransfer']);
        }
        if (!isset($model)) {
            throw new UserException('Fill the additional transfer form first');
        }

        $targetIdentifier = Yii::$app->request->post('name');
        $targetAmount = Yii::$app->request->post('value');

        if (!$model->validateTargetAmount($targetAmount)) {
            throw new UserException('Invalid target amount value');
        }
        $model->targetAmounts[$targetIdentifier] = $targetAmount;
        $model->refreshTotalAmount();
        Yii::$app->session['currentTransfer'] = serialize($model);
        return Json::encode(['status' => 'success', 'amount' => $model->amount]);
    }

    public function actionUpdateArrears()
    {
        $arrears = Yii::$app->apiClient->apiCall('getSummaryArrears', [
            'user_id' => Yii::$app->user->id,
        ])->arrears_info;

        list($relation, $targetUserId, $currency) = explode('_', Yii::$app->request->post('name'));
        $targetUserId = intval($targetUserId);
        $newAmount = Yii::$app->request->post('value');

        $oldAmount = array_reduce($arrears, function ($carry, \stdClass $item) use ($relation, $targetUserId, $currency) {
            if (!isset($carry) &&
                [$item->relation, $item->user_id, $item->currency] === [$relation, $targetUserId, $currency]
            ) {
                $carry = $item->amount;
            }
            return $carry;
        });

        if (!isset($oldAmount)) {
            throw new UserException('Specified row has not been found in the arrears list');
        }

        // TODO: REFACTOR IT PLEEZE!
        $amount = null;
        $sourceUserId = Yii::$app->user->id;
        if ($relation == 'creditor') {
            $amount = $oldAmount - $newAmount;
        } elseif ($relation == 'debtor') {
            $amount = $newAmount - $oldAmount;
        }
        if ($amount < 0) {
            $amount *= -1;
            $t = $sourceUserId;
            $sourceUserId = $targetUserId;
            $targetUserId = $t;
        }

        Yii::$app->apiClient->apiCall('createTransfer', [
            'source_user_id' => $sourceUserId,
            'target_user_ids' => [$targetUserId],
            'amount' => $amount,
            'currency' => $currency,
            'comment' => 'No comment (updated from summary arrears page)',
        ]);

//        $model = new CreateTransferForm();
//
//        $model->targetUsers = [$targetUserId];
//        if ($relation == 'creditor') {
//            $model->amount = $oldAmount - $newAmount;
//        } elseif ($relation == 'debtor') {
//            $model->amount = $newAmount - $oldAmount;
//        }
//        $model->fillTargetAmounts();
//        $model->currency = $currency;
//
//        if (!$model->createTransfer()) {
//            $firstError = $model->firstErrors;
//            $firstError = reset($firstError);
//            throw new UserException($firstError);
//        }

        return Json::encode(['status' => 'success']);
    }

    public function actionCreateTransfer()
    {
        $model = new CreateTransferForm();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax && Yii::$app->request->post('ajax') == 'create-transfer-form') {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            $model->fillTargetAmounts();

            if (isset(Yii::$app->request->bodyParams['additional-button'])) {
                Yii::$app->session['currentTransfer'] = serialize($model);
                return $this->redirect(['/site/create-transfer-additional']);
            }

            if ($model->createTransfer()) {
                unset(Yii::$app->session['currentTransfer']);
                return $this->redirect(['/site/summary-arrears']);
            }
        }

        return $this->render('createTransfer', [
            'model' => $model,
        ]);
    }

    public function actionCreateTransferAdditional()
    {
        if (!isset(Yii::$app->session['currentTransfer'])) {
            return $this->redirect(['/site/create-transfer']);
        }
        /** @var CreateTransferForm $model */
        $model = unserialize(Yii::$app->session['currentTransfer']);
        if (empty($model->targetUsers)) {
            return $this->redirect(['/site/create-transfer']);
        }
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax && Yii::$app->request->post('ajax') == 'create-additional-transfer-form') {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->createTransfer()) {
                unset(Yii::$app->session['currentTransfer']);
                return $this->redirect(['/site/summary-arrears']);
            }
        }
        return $this->render('createTransferAdditional', [
            'model' => $model,
        ]);
    }

    public function actionSummaryArrears()
    {
        $arrears = Yii::$app->apiClient->apiCall('getSummaryArrears', [
            'user_id' => Yii::$app->user->id,
        ])->arrears_info;

        return $this->render('summaryArrears', [
            'arrears' => $arrears,
        ]);
    }

    public function actionTransferLog()
    {
        $historyPages = new Pagination([
            'pageSize' => 10,
            'pageSizeParam' => false,
            'validatePage' => false,
        ]);

        $apiCallResult = Yii::$app->apiClient->apiCall('getTransferHistory', [
            'user_id' => Yii::$app->user->id,
            'limit' => $historyPages->limit,
            'offset' => $historyPages->offset,
        ]);

        $historyPages->totalCount = $apiCallResult->total_count;
        $history = $apiCallResult->transfer_history;

        return $this->render('transferLog', [
            'history' => $history,
            'historyPages' => $historyPages
        ]);
    }

//    public function actionFriendAutocomplete($part)
//    {
//        // TODO
//        if (!Yii::$app->request->isAjax) {
//            throw new BadRequestHttpException('This action should be performed via AJAX');
//        }
//
//        Yii::$app->response->format = Response::FORMAT_JSON;
//
//        $authClient = new VKontakte();
//        if (!$authClient->accessToken) {
//            return ['error' => 'No authentication provided'];
//        }
//        $authClientResponse = $authClient->api('friends.search', 'GET', ['q' => trim($part)]);
//
//        $items = $authClientResponse['response'];
//        array_shift($items);
//
//        $items = array_map(function (array $item) {
//            return $item['first_name'] . ' ' . $item['last_name'];
//        },
//            $items);
//
//        return ['result' => $items];
//    }
//
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
//                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
//            } else {
//                Yii::$app->session->setFlash('error', 'There was an error sending email.');
//            }
//
//            return $this->refresh();
//        } else {
//            return $this->render('contact', [
//                'model' => $model,
//            ]);
//        }
//    }
//
//    public function actionAbout()
//    {
//        return $this->render('about');
//    }
//
//    public function actionSignup()
//    {
//        $model = new SignupForm();
//        if ($model->load(Yii::$app->request->post())) {
//            if ($user = $model->signup()) {
//                if (Yii::$app->getUser()->login($user)) {
//                    return $this->goHome();
//                }
//            }
//        }
//
//        return $this->render('signup', [
//            'model' => $model,
//        ]);
//    }
//
//    public function actionRequestPasswordReset()
//    {
//        $model = new PasswordResetRequestForm();
//        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            if ($model->sendEmail()) {
//                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
//
//                return $this->goHome();
//            } else {
//                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
//            }
//        }
//
//        return $this->render('requestPasswordResetToken', [
//            'model' => $model,
//        ]);
//    }
//
//    public function actionResetPassword($token)
//    {
//        try {
//            $model = new ResetPasswordForm($token);
//        } catch (InvalidParamException $e) {
//            throw new BadRequestHttpException($e->getMessage());
//        }
//
//        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
//            Yii::$app->getSession()->setFlash('success', 'New password was saved.');
//
//            return $this->goHome();
//        }
//
//        return $this->render('resetPassword', [
//            'model' => $model,
//        ]);
//    }
}

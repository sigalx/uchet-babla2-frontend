<?php

return [
    'Use your social network profile to login into the application' =>
        'Зарегистрируйтесь с помощью аккаунта в социальной сети',

    'Contact administrator' => 'Написать администратору',

    'My creditors:' => 'Кому я должен:',
    'My debtors:' => 'Кто мне должен:',

    'Name' => 'Имя',
    'Source' => 'Источник',
    'Target' => 'Получатель',
    'Date' => 'Дата',
    'Amount' => 'Сумма',
    'Currency' => 'Валюта',
    'Comment' => 'Комментарий',

    'Login' => 'Войти',
    'Logout' => 'Выйти',
    'Create Transfer' => 'Создать учёт',
    'Transfer Log' => 'История учётов',
    'Summary Arrears' => 'Общий учёт',

    'Please fill out the following fields to create a transfer:' => 'Заполните обязательные поля:',
    'Select recipients' => 'Выберите друзей',
    'Type amount of the transfer' => 'Введите сумму для учёта',
    'Type comment' => 'Напишите комментарий',

    'Friends' => 'Друзья',

    'Create transfer' => 'Создать учёт',
    'Additional options' => 'Больше возможностей',

    'Transfer amount:' => 'Сумма учёта',
];

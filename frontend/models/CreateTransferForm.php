<?php

namespace frontend\models;

use Yii;
use yii\authclient\clients\VKontakte;
use yii\authclient\OAuth2;
use yii\base\Exception;
use yii\base\Model;

use yii\validators\NumberValidator;

use yii\helpers\Html;
use yii\helpers\Json;

use frontend\components\ApiCallException;

/**
 * Class CreateTransferForm
 * @package frontend\models
 *
 * @property array $friendList
 */
class CreateTransferForm extends Model
{
    /**
     * @var string[]|integer[]
     */
    public $targetUsers;

    /**
     * @var integer[]
     */
    public $targetAmounts;

    /**
     * @var double
     */
    public $amount;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var string
     */
    public $comment;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['targetUsers', 'amount', 'currency', 'comment'], 'required'],
            ['amount', 'number', 'min' => 0.1],
//            ['targetUsers', 'checkTargetUsers'],
//            ['targetAmounts', 'checkTargetAmounts'],
//            ['amount', 'checkAmount'],
//            ['currency', 'checkCurrency'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'targetUsers' => Yii::t('app', 'Friends'),
            'amount' => Yii::t('app', 'Amount'),
            'currency' => Yii::t('app', 'Currency'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    public function validateTargetAmount($targetAmount)
    {
        $error = null;
        $v = new NumberValidator();
        $v->validate($targetAmount, $error);
        return !isset($error);
    }

    public function refreshTotalAmount()
    {
        $this->amount = array_reduce($this->targetAmounts, function ($carry, $x) {
            $carry += $x;
            return $carry;
        }, 0);
    }

    public function fillTargetAmounts()
    {
        if (isset($this->targetAmounts)) {
            return;
        }
        foreach ($this->targetUsers as $targetIdentifier) {
            $this->targetAmounts[$targetIdentifier] = round($this->amount / count($this->targetUsers));
        }
    }

    public function getAvailableCurrencies()
    {
        return Yii::$app->apiClient->apiCall('getAvailableCurrencies')->currency_list;
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    public function getFriendListIdentified()
    {
        $result = array();
        foreach ($this->getFriendListStructured() as $userGroup) {
            foreach ($userGroup['users'] as $user) {
                $result[$user['identifier']] = $user;
            }
        }
        return $result;
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    public function getFriendListStructured()
    {
        $friendList = [[
            'name' => '',
            'users' => [$this->getAuthProfileInfo()],
        ]];

        /** @var \yii\authclient\Collection $authClientCollection */
        $authClientCollection = Yii::$app->authClientCollection;
        /** @var \yii\authclient\clients\VKontakte $authClient */
        $authClient = $authClientCollection->clients['vkontakte'];
        if ($authClient->accessToken) {
            $authClientResponse = $authClient->api('friends.get', 'GET', ['fields' => 'id, name, photo_100', 'order' => 'name']);

            $items = $authClientResponse['response'];
            array_shift($items);

            $userGroup = [
                'name' => 'VK.com',
                'users' => [],
            ];

            $userGroup['users'] = array_reduce($items, function (array $carry, array $item) use ($authClient) {
                $newItem['identifier'] = '--' . $authClient->getId() . '--' . $item['user_id'];
                $newItem['username'] = $item['first_name'] . ' ' . $item['last_name'];
                $newItem['userpic'] = $item['photo_100'];
                $carry[] = $newItem;
                return $carry;
            },
                []);

            $friendList['vkontakte'] = $userGroup;
        }

//        $authClient = new GoogleOAuth();
//        if ($authClient->accessToken) {
//            $authClientResponse = $authClient->api('people/me/people/visible', 'GET', ['orderBy' => 'best']);
//
//            $items = $authClientResponse['items'];
//
//            $userGroup = [
//                'name' => 'Google+',
//                'users' => [],
//            ];
//
//            $userGroup['users'] = array_reduce($items, function (array $carry, array $item) use ($authClient) {
//                $newItem['identifier'] = '--' . $authClient->getId() . '--' . $item['id'];
//                $newItem['username'] = Html::encode($item['displayName']);
//                $newItem['userpic'] = $item['image']['url'];
//                $carry[] = $newItem;
//                return $carry;
//            },
//                []);
//
//            $friendList['google'] = $userGroup;
//        }

        return $friendList;
    }

    public function getAuthProfileInfo()
    {
        $result['identifier'] = Yii::$app->user->id;
        $result['username'] = Yii::$app->user->identity->username;

        /** @var \yii\authclient\Collection $authClientCollection */
        $authClientCollection = Yii::$app->authClientCollection;
        /** @var \yii\authclient\clients\VKontakte $authClient */
        $authClient = $authClientCollection->clients['vkontakte'];
        if ($authClient->accessToken) {
            $authClientResponse = $authClient->api('users.get', 'GET', ['fields' => 'photo_100']);

            $authClientResponse = $authClientResponse['response'];
            $authClientResponse = reset($authClientResponse);

            $result['userpic'] = $authClientResponse['photo_100'];
        }

//        $authClient = new GoogleOAuth();
//        if ($authClient->accessToken) {
//            $authClientResponse = $authClient->api('people/me/people/visible', 'GET', ['orderBy' => 'best']);
//
//            // TODO
//
//            $result['userpic'] = '';
//        }

        return $result;
    }

    public function createTransfer()
    {
        if (!$this->validate()) {
            return false;
        }

        $targetUserIds = $this->targetUsers;

        // TODO: make a batch
        $that = $this;
        $targetUserIds = array_map(function ($targetIdentifier) use (&$that) {
            if (substr($targetIdentifier, 0, 2) != '--') {
                return $targetIdentifier;
            }
            list($authSource, $authSourceId) = explode('--', substr($targetIdentifier, 2));
            $targetUserId = null;
            try {
                $authService = Yii::$app->apiClient->apiCall('findAuthServiceLink', [
                    'source' => $authSource,
                    'source_id' => $authSourceId,
                ]);
                $targetUserId = $authService->user_info->id;
            } catch (ApiCallException $apiCallException) {
                $newUser = null;
                $login = $authSource . '-' . $authSourceId;
                $username = null;

                if ($authSource == 'vkontakte') {
                    $authClient = new VKontakte();
                    if (!$authClient->accessToken) {
                        return Yii::$app->user->loginRequired();
                    }
                    $authClientResponse = $authClient->api('users.get', 'GET', ['user_ids' => $authSourceId, 'fields' => 'first_name, last_name']);

                    $authClientResponse = $authClientResponse['response'];
                    $authClientResponse = reset($authClientResponse);

                    $username = $authClientResponse['first_name'] . ' ' . $authClientResponse['last_name'];
                }

                try {
                    $newUser = Yii::$app->apiClient->apiCall('createUser', [
                        'login' => $login,
                        'username' => $username,
                    ]);
                } catch (ApiCallException $apiCallException) {
                    $newUser = Yii::$app->apiClient->apiCall('createUser', [
                        'login' => $login . '-' . substr(md5($login), 0, 6),
                        'username' => $username,
                    ]);
                }
                Yii::$app->apiClient->apiCall('createAuthServiceLink', [
                    'user_id' => $newUser->user_info->id,
                    'source' => $authSource,
                    'source_id' => $authSourceId,
                ]);
                $targetUserId = $newUser->user_info->id;
            }
            $this->targetAmounts[$targetUserId] = $this->targetAmounts[$targetIdentifier];
            return $targetUserId;
        }, $targetUserIds);

        if (!is_array($targetUserIds)) {
            $targetUserIds = [$targetUserIds];
        }

        foreach ($targetUserIds as $targetUserId) {
            if (Yii::$app->user->id == $targetUserId) {
                continue;
            }
            Yii::$app->apiClient->apiCall('createTransfer', [
                'source_user_id' => Yii::$app->user->id,
                'target_user_ids' => [$targetUserId],
                'amount' => $this->targetAmounts[$targetUserId],
                'currency' => $this->currency,
                'comment' => $this->comment,
            ]);
        }

        return true;
    }
}

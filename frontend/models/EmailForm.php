<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Email form
 */
class EmailForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }
}

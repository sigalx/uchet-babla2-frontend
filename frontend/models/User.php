<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Exception;
use yii\base\Model;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *
 * @property \stdClass $userInfo
 */
class User extends Model implements IdentityInterface
{
    /**
     * @var \stdClass
     */
    protected $_userInfo;

    /**
     * @param \stdClass $userInfo
     */
    public function setUserInfo(\stdClass $userInfo)
    {
        $this->_userInfo = $userInfo;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $userInfo = Yii::$app->apiClient->apiCall('getUserInfo', [
            'user_id' => $id,
        ]);
        $userInfo->user_info->id = $id;
        $identity = new static;
        $identity->setUserInfo($userInfo->user_info);
        return $identity;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        if (!$this->_userInfo) {
            return null;
        }
        return !empty($this->_userInfo->id) ? $this->_userInfo->id : null;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        throw new NotSupportedException('"getAuthKey" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        throw new NotSupportedException('"validateAuthKey" is not implemented.');
    }

    public function getUserInfo()
    {
        if (!$this->_userInfo) {
            throw new Exception('UserInfo has not been set.');
        }
        return $this->_userInfo;
    }

    public function getUsername()
    {
        return $this->userInfo->username;
    }
}

<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

use nex\chosen\Chosen;

use frontend\helpers\CurrencyHelper;

/** @var $this yii\web\View */
/** @var $model \frontend\models\CreateTransferForm */

$this->title = Yii::t('app', 'Create Transfer');
$this->params['breadcrumbs'][] = $this->title;

?>
<p><?= Yii::t('app', 'Please fill out the following fields to create a transfer:') ?></p>

<div class="row">
    <?php $form = ActiveForm::begin([
        'id' => 'create-transfer-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'fieldConfig' => [
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ],
    ]); ?>

    <?= $form->field($model, 'targetUsers', [
        'template' => <<<'EOD'
                <div class="form-group field-createtransferform-targetUsers">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group full-width">
                                <span class="input-group-addon">@</span>
                                {input}
                            </div>
                        </div>
                        <div class="col-md-6">{error}</div>
                    </div>
                </div>
EOD
        ,
        'inputOptions' => [
        ],
    ])->widget(Chosen::className(), [
        'items' => [],
        'multiple' => true,
        'clientOptions' => [
            'search_contains' => true,
            'single_backstroke_delete' => false,
            'max_selected_options' => 20,
        ],
        'options' => [
            'placeholder' => Yii::t('app', 'Select recipients'),
            'data-placeholder' => Yii::t('app', 'Select recipients'),
            'aria-describedby' => 'basic-addon1',
        ],
    ]); ?>

    <?= Html::activeHiddenInput($model, 'currency') ?>

    <?= $form->field($model, 'amount', [
        'template' => <<<EOD
                <div class="row">
                    <div class="col-md-6">
                        <div class="dropdown">
                            <div class="input-group full-width">
                                <div class="btn btn-default dropdown-toggle input-group-addon" type="button"
                                    id="currency-dropdown-menu" data-toggle="dropdown" aria-expanded="true"></div>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="currency-dropdown-menu"
                                    id="currency-dropdown-menu-items"></ul>
                                {input}
                                <span class="input-group-addon">.00</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">{error}</div>
                </div>
EOD
        ,
        'inputOptions' => [
            'type' => 'number',
            'placeholder' => Yii::t('app', 'Type amount of the transfer'),
            'aria-label' => Yii::t('app', 'Type amount of the transfer'),
        ]]) ?>

    <?= $form->field($model, 'comment', [
        'template' => <<<EOD
                <div class="row">
                    <div class="col-md-6">{input}</div>
                    <div class="col-md-6">{error}</div>
                </div>
EOD
        ,
        'inputOptions' => [
            'type' => 'text',
            'placeholder' => Yii::t('app', 'Type comment'),
            'aria-label' => Yii::t('app', 'Comment'),
        ]]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create transfer'), ['class' => 'btn btn-primary', 'name' => 'create-transfer-button']) ?>
        <?= Html::submitButton(Yii::t('app', 'Additional options'), ['class' => 'btn', 'name' => 'additional-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php /* TODO */
$currencyList = array_reduce($model->getAvailableCurrencies(),
    function ($carry, $currencyCode) {
        $carry[] = [
            'code' => $currencyCode,
            'symbol' => CurrencyHelper::getCurrencySymbol($currencyCode),
        ];
        return $carry;
    }, array());

$this->registerJs('var relatedUsers = ' . \yii\helpers\Json::encode($model->getFriendListStructured()) . ';', \yii\web\View::POS_END);
$this->registerJs('var selectedUsers = ' . \yii\helpers\Json::encode($model->targetUsers) . ';', \yii\web\View::POS_END);
$this->registerJs('var currencyList = ' . \yii\helpers\Json::encode($currencyList) . ';', \yii\web\View::POS_END);

$this->registerJs(<<< 'JS'
    var relatedUserSelector = $('#createtransferform-targetusers');
    $.each(relatedUsers, function(groupIndex, groupData) {
        var elemOptgroup = $('<optgroup>');
        elemOptgroup.attr('label', groupData.name);
        $.each(groupData.users, function(groupUserIndex, groupUser) {
            var elemOption = $('<option>');
            elemOption.attr('value', groupUser.identifier);
            elemOption.attr('data-img-src', groupUser.userpic);
            if ($.inArray(groupUser.identifier, selectedUsers) + 1) {
                elemOption.attr('selected', 'selected');
            }
            elemOption.text(groupUser.username);
            elemOptgroup.append(elemOption);
        });
        relatedUserSelector.append(elemOptgroup);
    });
    relatedUserSelector.trigger("chosen:updated");
JS
    , \yii\web\View::POS_READY);
$this->registerJs(<<< 'JS'
    function setCurrentCurrency(n) {
        $('#createtransferform-currency').val(currencyList[n].code);
        $('#currency-dropdown-menu').html(currencyList[n].symbol + ' <span class="caret"></span>');
    }

    var currencyDropdownMenuItems = $('#currency-dropdown-menu-items');
    for (var n in currencyList) {
        var menuItem = $('<li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-index="'
            + n + '">' + currencyList[n].symbol + '</a></li>');
        currencyDropdownMenuItems.append(menuItem);
    }

    $('ul#currency-dropdown-menu-items > li').on('click', function(event){
        setCurrentCurrency($(event.target).attr('data-index'));
    });

    setCurrentCurrency(0);
JS
    , \yii\web\View::POS_READY);
?>

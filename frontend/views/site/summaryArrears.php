<?php
/** @var stdClass[] $arrears */

$this->title = Yii::t('app', 'Summary Arrears');
$this->params['breadcrumbs'][] = $this->title;

use \dosamigos\editable\Editable;

?>

<div class="row">
    <div class="col-md-6">
        <h4><?= Yii::t('app', 'My creditors:') ?></h4>
        <table class="table table-striped ub-arrears-table">
            <thead>
            <tr>
                <th><?= Yii::t('app', 'Name') ?></th>
                <th><?= Yii::t('app', 'Amount') ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach (array_filter($arrears, function (\stdClass $item) {
                return $item->relation == 'creditor';
            }) as $row): ?>
                <tr>
                    <td><?= $row->username ?></td>
                    <td>
                        <?= Editable::widget([
                            'type' => 'text',
                            'name' => $row->relation . '_' . $row->user_id . '_' . $row->currency,
                            'value' => $row->amount,
                            'url' => 'update-arrears',
                            'clientOptions' => [
//                                'success' => new \yii\web\JsExpression('onTargetAmountUpdated'),
                            ]
                        ]) ?></td>
                    <td><?= \frontend\helpers\CurrencyHelper::getCurrencySymbol($row->currency) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h4><?= Yii::t('app', 'My debtors:') ?></h4>
        <table class="table table-striped ub-arrears-table">
            <thead>
            <tr>
                <th><?= Yii::t('app', 'Name') ?></th>
                <th><?= Yii::t('app', 'Amount') ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach (array_filter($arrears, function (\stdClass $item) {
                return $item->relation == 'debtor';
            }) as $row): ?>
                <tr>
                    <td><?= $row->username ?></td>
                    <td>
                        <?= Editable::widget([
                            'type' => 'text',
                            'name' => $row->relation . '_' . $row->user_id . '_' . $row->currency,
                            'value' => $row->amount,
                            'url' => 'update-arrears',
                            'clientOptions' => [
//                                'success' => new \yii\web\JsExpression('onTargetAmountUpdated'),
                            ]
                        ]) ?></td>
                    <td><?= \frontend\helpers\CurrencyHelper::getCurrencySymbol($row->currency) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


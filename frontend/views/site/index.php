<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1><?= Yii::$app->name ?></h1>

        <p class="lead">Submit your email address to receive important notifications just in time</p>

        <div class="ub-index-email-form">
            <?php $form = ActiveForm::begin(['id' => 'email-form', 'action' => ['/site/set-email']]); ?>
            <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'john.doe@example.com']]) ?>
            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-success green', 'name' => 'submit-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

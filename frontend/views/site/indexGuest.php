<?php
/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>
<div class="site-index">
    <div class="jumbotron">
        <h1><?= Yii::$app->name ?></h1>

        <p class="lead"><?= Yii::t('app', 'Use your social network profile to login into the application') ?></p>

        <div class="ub-index-auth-clients">
            <?= yii\authclient\widgets\AuthChoice::widget([
                'baseAuthUrl' => ['site/auth']
            ]) ?>
        </div>
    </div>
</div>

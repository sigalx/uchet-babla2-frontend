<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\helpers\CurrencyHelper;

use \dosamigos\editable\Editable;

/** @var $this yii\web\View */
/** @var $model \frontend\models\CreateTransferForm */

$this->title = Yii::t('app', 'Create Transfer');
$this->params['breadcrumbs'][] = $this->title;

$friendListIdentified = $model->getFriendListIdentified();

?>

<div class="row">

    <?php $form = ActiveForm::begin([
        'id' => 'create-additional-transfer-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'fieldConfig' => [
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ],
    ]); ?>

    <?php foreach ($model->targetUsers as $targetIdentifier): ?>
        <div class="row">
            <div class="col-md-4">
                <div class="ub-additional-target-user-box">
                    <?= Html::img($friendListIdentified[$targetIdentifier]['userpic'], ['class' => 'ub-medium-userpic']) ?>
                    <div class="ub-additional-target-user-box-text">
                        <?= $friendListIdentified[$targetIdentifier]['username'] ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4 lead ub-additional-target-user-box-text">
                <span data-type="target-user-amount" data-identifier="<?= $targetIdentifier ?>">
                    <?= Editable::widget([
                        'type' => 'text',
                        'name' => $targetIdentifier,
                        'value' => $model->targetAmounts[$targetIdentifier],
                        'url' => 'update-target-amount',
                        'clientOptions' => [
                            'success' => new \yii\web\JsExpression('onTargetAmountUpdated'),
                        ]
                    ]) ?></span>
                <span><?= CurrencyHelper::getCurrencySymbol($model->currency) ?></span>
            </div>
        </div>
    <?php endforeach; ?>

    <div class="row">
        <div class="col-md-4 lead"><?= Yii::t('app', 'Transfer amount:') ?></div>
        <div class="col-md-4 lead">
            <span data-type="total-amount"><?= $model->amount ?></span>
            <?= CurrencyHelper::getCurrencySymbol($model->currency) ?></div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'comment', []) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= Html::submitButton(Yii::t('app', 'Create transfer'), ['class' => 'btn btn-primary', 'name' => 'create-transfer-button']) ?>
        <div class="btn"><?= Html::a(Yii::t('app', 'Назад'), ['site/create-transfer']) ?></div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php $currencyList = array_reduce($model->getAvailableCurrencies(),
    function ($carry, $currencyCode) {
        $carry[] = [
            'code' => $currencyCode,
            'symbol' => CurrencyHelper::getCurrencySymbol($currencyCode),
        ];
        return $carry;
    }, array());

$this->registerJs('var transferAmount = ' . \yii\helpers\Json::encode($model->amount) . ';', \yii\web\View::POS_END);
$this->registerJs(<<< 'JS'
    function onTargetAmountUpdated(response, newValue) {
        response = $.parseJSON(response);
        $('[data-type="total-amount"]').text(response.amount);
    }
JS
    , \yii\web\View::POS_END);
$this->registerJs(<<< 'JS'
JS
    , \yii\web\View::POS_READY);
?>

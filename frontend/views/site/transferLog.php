<?php
/** @var stdClass[] $history */
/** @var Pag title */

$this->title = Yii::t('app', 'Transfer Log');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <table class="table table-striped ub-transfer-log">
        <thead>
        <tr>
            <th><?= Yii::t('app', 'Date') ?></th>
            <th><?= Yii::t('app', 'Source') ?></th>
            <th><?= Yii::t('app', 'Target') ?></th>
            <th><?= Yii::t('app', 'Amount') ?></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($history as $item): ?>
            <tr>
                <td><?= Yii::$app->formatter->asDate($item->timestamp, 'long') ?></td>
                <td><?= $item->source_username ?></td>
                <td><?= $item->target_username ?></td>
                <td><?= $item->amount ?></td>
                <td><?= \frontend\helpers\CurrencyHelper::getCurrencySymbol($item->currency) ?></td>
                <td><?= $item->comment ?></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>

<?= \yii\widgets\LinkPager::widget(['pagination' => $historyPages]) ?>

<?php

namespace frontend\components;

use yii\base\Exception;

class ApiCallException extends Exception
{
    public function getName()
    {
        return 'API call exception';
    }
}

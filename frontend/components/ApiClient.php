<?php

namespace frontend\components;

use Yii;
use yii\base\Component;
use yii\base\Exception;

class ApiClient extends Component
{
    /**
     * @var \JsonRpc\Client
     */
    protected $_client;

    public function __construct()
    {
        $this->_client = new \JsonRpc\Client('http://dev.home.sigalx.ru/webdev/uchet-babla/www/htdocs/index.php?r=api');
    }

    /**
     * @param string $method
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    public function apiCall($method, array $params = [])
    {
        if (!$this->_client->call($method, $params)) {
            throw new ApiCallException($this->_client->error, $this->_client->errorCode);
        }

        return $this->_client->result;
    }

    /**
     *
     */
    public function apiOpenBatch()
    {
        $this->_client->batchOpen();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function apiSendBatch()
    {
        return $this->_client->batchSend();
    }

}

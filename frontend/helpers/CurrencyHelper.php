<?php

namespace frontend\helpers;

class CurrencyHelper
{
    protected static function getCurrencyInfo($currencyCode = null)
    {
        if ($currencyCode) {
            $knownCurrencies = static::getCurrencyInfo();
            if (isset($knownCurrencies[$currencyCode])) {
                return $knownCurrencies[$currencyCode];
            } else {
                return null;
            }
        }
        return [
            'RUB' => [
                'symbol' => '\u20BD',
                'htmlSymbol' => '&#x20BD;',
            ],
            'USD' => [
                'symbol' => '$',
            ],
            'EUR' => [
                'symbol' => '€',
            ],
        ];
    }

    /**
     * @param string $currencyCode
     * @return string
     */
    public static function getCurrencySymbol($currencyCode)
    {
        $result = $currencyCode;
        if ($currencyInfo = static::getCurrencyInfo($currencyCode)) {
            if (isset($currencyInfo['symbol'])) {
                $result = $currencyInfo['symbol'];
            }
            if (isset($currencyInfo['htmlSymbol'])) {
                $result = $currencyInfo['htmlSymbol'];
            }
        }
        return $result;
    }

}

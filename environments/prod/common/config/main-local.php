<?php
return [
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'clientId' => 0,
                    'clientSecret' => 'xxx',
                ],
            ],
        ],
        'apiClient' => [
            // TODO
        ],
    ],
    'modules' => [
    ],
];

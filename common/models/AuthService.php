<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $source
 * @property string $source_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class AuthService extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%auth_services}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source'], 'in', 'range' => [
                'vkontakte',
                'google'
            ]],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param integer $userId
     * @return null|static
     */
    public static function findByUserId($userId)
    {
        return static::findOne(['user_id' => $userId]);
    }
}

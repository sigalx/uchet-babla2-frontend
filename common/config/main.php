<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'urlManager' => [
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'scope' => implode(',', [
                        'friends',
                    ]),
                ],
            ],
        ],
        'apiClient' => [
            'class' => 'frontend\components\ApiClient',
        ],
    ],
    'modules' => [
    ],
];
